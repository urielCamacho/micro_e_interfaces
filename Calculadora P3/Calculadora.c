//************************************************************************************************
// Lee un nunero de un teclado  4x4  conectado al puerto B y lo muestra en el LCD conectado al puerto D
// Drive para teclado  4x4 
// Para simular en proteus usar resistencias externas.
//JUN 16,oct16
//***********CONFIGURACION DEL PIC ************************************************************************

#include <18f4550.h>
                                    
#fuses HS,NOWDT,NOPROTECT,NOLVP,PUT,CPUDIV1,BROWNOUT    // Palabra de Configuración
//#fuses NOPBADEN                  //configura  I/O digitales.
#use delay(clock=20M)                           //Velocidad del reloj externo, 12 mhz

#include <lcd.c>                  //  Display
#include <kbd_4x4.c>             //Libreria para el manejo de un Teclado 4x4
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
//#include <KBD.c>                //  Teclado 3x4
//#include <kbd_4x4_ResExt.c>        //ok Proteus 8


//#define use_portb_kbd TRUE     // Puerto B  a utilizar para el Teclado Matricial
#define use_portd_lcd TRUE     // Puerto D  a utilizar para el LCD

//************************  PROGRAMA  ************************

void main()
{   

char k;
float n1, n2, res;
int c, op, b;
lcd_init();
kbd_init();

port_b_pullups(TRUE);
c=0; op=0; b=0;
   printf(lcd_putc,"Calculadora");
   delay_ms(5000);
   lcd_putc('\f');
   while (TRUE)
   {
   k=kbd_getc();

   if(k!=0)
      {
         if(k=='#'){
          lcd_putc('\f');
          c=0;
         }
         else{ 
            c++;
            lcd_gotoxy(c,1);
            printf(lcd_putc, "%c",k);
            if(k=='+') op=1;
            else if(k=='-') op=2;
            else if(k=='*') op=3;
            else if(k=='/') op=4;
            else if(k=='=') b=1;
            if(op==0) n1=k-'0'; 
            else if(op>0 && b==0) n2=k-'0'; 
            if(b==1){ 
               b=0;
               if(op==1) res=n1+n2;
               else if(op==2) res=n1-n2;
               else if(op==3) res=n1*n2;
               else if(op==4) res=n1/n2;
               op=0;
               lcd_gotoxy(1,2);
               printf(lcd_putc, "Resultado: %f",res);
               c=0;
            }
            delay_ms(300);
         }
             
      }//if k  

    }//while  
}

