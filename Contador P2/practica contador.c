#include <18F4550.h>
#USE DELAY( CLOCK=4000000)
#FUSES XT,NOWDT,NOPROTECT,NOPUT
#USE fast_IO (B)
#USE fast_IO (A)
byte CONST DISPLAY[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};

main(){
byte ud=0,dec=0, cen=0, mil=0;
SET_TRIS_B(0x00);
SET_TRIS_A(0x00);
OUTPUT_B(0);
while(1){
   for (mil=0; mil<10; mil++){
      for (cen=0; cen<10; cen++){
         for (dec=0;dec<10;dec++){       //Cuenta digito decenas
           for (ud=0;ud<10;ud++){
               OUTPUT_A(0x0E);//1110 -> cat_M=pagado, cat_C=apagado, cat_D=apagado, cat_U=encendido
               OUTPUT_B(DISPLAY[ud]);//Digito unidades
               delay_ms(10);    //Para evitar parpadeos
         
               if(dec==0 && cen==0 && mil==0) output_a(0x0F); 
               else output_a(0x0D);           //Si decenas>0, cat_D=encendido
                  
               OUTPUT_B(DISPLAY[dec]);      //Digito decenas
               delay_ms(10);  //Para evitar parpadeos
               
               if(cen==0 && mil==0) output_a(0x0F);
               else output_a(0x0B);
               
               OUTPUT_B(DISPLAY[cen]);
               delay_ms(10);
               
               if(mil==0) output_a(0x0F);
               else output_a(0x07);
               
               OUTPUT_B(DISPLAY[mil]);
               delay_ms(10);
            }
         }
      }
   }
 }
}

