


//************************************************************************************************
// Entradas y salidas                                                    PRUEBA3
// Lee el puerto B  y lo uestra en el puerto D
// Define  los REGISTROS ESPECIALES el compilador de C
// ago15/
//*************************************************************************************************
#include <18F4550.h>// Indica el pic de trabajo
#include <math.h>
#FUSES HS,NOWDT                    //No Watch Dog Timer
#FUSES BROWNOUT  // brownout reset caida de voltaje
#include <PIC18F4550_registers.h> 
#use delay(crystal=12MHz)       //Velocidad del reloj  de 12 mhz

//*****************************************************************
void main()
{
 TRISD = 0x00;         //Definimos el puerto D como salida  "0" salida
 TRISB = 0xff;         //Definimos el puerto B como entrada"1" entrada,
 port_b_pullups(true);
 float x;
   while(1)                  // ciclo infinito
    { 
         x = PORTB;
         PORTD = pow(x,2);  // lee el puerto B y lo muestra en el puerto D
    }  
}


