


//************************************************************************************************
// Entradas y salidas                                         Prueba1
// Lee el puerto B  y lo uestra en el puerto D
// Se definen los REGISTROS ESPECIALES como localidades de memoria  
// Se definen los REGISTROS ESPECIALES como localidades de memoria   en un archivo .h
// ago15/
//*************************************************************************************************
#include <18F4550.h>             // Indica el pic de trabajo
#FUSES HS,NOWDT                    //No Watch Dog Timer
#FUSES BROWNOUT                 // brownout reset caida de voltaje
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#use delay(crystal=20MHz)       //Velocidad del reloj  de 12 mhz

//*********************************************************************************
#include <PIC18F4550_registers.h>             // definimos la direccion de los  registros de trabajo 
/*
#byte TRISE = 0xF96
#byte TRISD = 0xF95
#byte TRISC = 0xF94
#byte TRISB = 0xF93
#byte TRISA = 0xF92

#byte PORTE = 0xF84
#byte PORTD = 0xF83
#byte PORTC = 0xF82
#byte PORTB = 0xF81
#byte PORTA = 0xF80 */

//*****************************************************************
void main()
{
 TRISD = 0x00;            //Definimos el puerto D como salida  "0" salida
 TRISB = 0xFF;            //Definimos el puerto B como entrada"1" entrada,
 port_b_pullups(true);    //Resistencias internas
 
 while(1)                  // ciclo infinito
    {
       PORTD= PORTB;                                
    }
    
}


