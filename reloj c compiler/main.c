#include <16f877a.h>                                //Incluye la Libreria de Pic a Utilizar
#use delay(clock=20000000)                          //Cristal externo de 20MHz
#include <lcd.c>                                    //Incluye la Libreria del LCD
#fuses HS,NOWDT                                     //HS para cristales de alta frecuencia y NOWDT apaga el perro guardian
#define use_portd_lcd TRUE                          //Define Puertas "D" Para salidas hacia el LCD
      
      
void  main () {  
                                                      //comienza el programa
                                                      //variable de el reloj
     int  i=0;                                        //milecimas de segundo 
     int  s=0;                                        //segundos
     int  m=0;                                        //minutos
     int  h=0;                                        //horas
    
    lcd_init();                                       // expecifica que trabaja con la lcd

 while (true)                                         //constante que expecifica que el programa se repita una ves que termine 
 {
     lcd_gotoxy(1,1);                                 //pocicion de la lcd linea1 caracter1
     lcd_gotoxy(1,2);                                 //pocicion de la lcd caracter1, linea 2
   
     printf (lcd_putc,"%02d:%02d:%02d",h,m,s);        // texto transmitido (reloj)
    
     i++;                                             //da inicio a que las milecimas cuenten                                       
     if(i==100){                                      //es la cantidad de veces que esta variable estara contando
     i=00;                                            //comienza desde 00 a contar 
     s++;}                                            //al terminar las milesimas salta a contar los segundos 
     if(s==60){                                       //cantidad de veces que cuenta la variable
     s=00;                                            //comiensa desde 00a contar 
     m++;                                             //termina los segundos y salta a los minutos 
     }
     if(m==60){                                       //cantidad de veces que cuenta la variable 
     m=00;                                            //comienza desde 00 a contar 
     output_high(PIN_C0);                             //cuando los minutos llegen a 60 manda un pulso alto a la salida c0
     h++;                                             //salta a las horas
     }
     if(h==24){                                       // cantidad de veces que la variable contara 
     h=00;                                            // comienza desde 00
     s++;}                                            //se repite el programa desde los segundos 
    
   if(input(pin_b2)==1){                              // al momento de tener un pulso alto en b2 sumara los segundos para aumentar los minutos
   s++;}
   if(input(pin_b1)==1){                              // al momento de tener un pulso alto en b1 sumara los minutos para aumentar las horas 
   m++;}
   
   if(s==2){                                          // manda un pulso bajo para apagar el led que se enciende cuando pasa una hora 
     output_low(PIN_C0);}
   
       
 }}
  
