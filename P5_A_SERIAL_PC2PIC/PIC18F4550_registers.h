#byte TOSU = 0xFFF
#word TOS = 0xFFE
#byte STKPTR = 0xFFC
#byte STKPTR = 0xFFC
#byte PCLATU = 0xFFB
#byte PCLATH = 0xFFA
#byte PCL = 0xFF9
#byte TBLPTRU = 0xFF8
#word TBLPTR = 0xFF7
#byte TABLAT = 0xFF5
#word PROD = 0xFF4
#byte INTCON = 0xFF2
#byte INTCON = 0xFF2
#byte INTCON = 0xFF2
#byte INTCON2 = 0xFF1
#byte INTCON2 = 0xFF1
#byte INTCON3 = 0xFF0
#byte INTCON3 = 0xFF0
#byte INDF0 = 0xFEF
#byte POSTINC0 = 0xFEE
#byte POSTDEC0 = 0xFED
#byte PREINC0 = 0xFEC
#byte PLUSW0 = 0xFEB
#word FSR0 = 0xFEA
#byte WREG = 0xFE8
#byte INDF1 = 0xFE7
#byte POSTINC1 = 0xFE6
#byte POSTDEC1 = 0xFE5
#byte PREINC1 = 0xFE4
#byte PLUSW1 = 0xFE3
#word FSR1 = 0xFE2
#byte BSR = 0xFE0
#byte INDF2 = 0xFDF
#byte POSTINC2 = 0xFDE
#byte POSTDEC2 = 0xFDD
#byte PREINC2 = 0xFDC
#byte PLUSW2 = 0xFDB
#word FSR2 = 0xFDA
#byte STATUS = 0xFD8
#word TMR0 = 0xFD7
#byte T0CON = 0xFD5
#byte OSCCON = 0xFD3
#byte OSCCON = 0xFD3
#byte HLVDCON = 0xFD2
#byte HLVDCON = 0xFD2
#byte HLVDCON = 0xFD2
#byte WDTCON = 0xFD1
#byte WDTCON = 0xFD1
#byte RCON = 0xFD0
#word TMR1 = 0xFCF
#byte T1CON = 0xFCD
#byte TMR2 = 0xFCC
#byte PR2 = 0xFCB
#byte T2CON = 0xFCA
#byte T2CON = 0xFCA
#byte SSPBUF = 0xFC9
#byte SSPADD = 0xFC8
#byte SSPSTAT = 0xFC7
#byte SSPSTAT = 0xFC7
#byte SSPSTAT = 0xFC7
#byte SSPSTAT = 0xFC7
#byte SSPSTAT = 0xFC7
#byte SSPSTAT = 0xFC7
#byte SSPCON1 = 0xFC6
#byte SSPCON2 = 0xFC5
#word ADRES = 0xFC4
#byte ADCON0 = 0xFC2
#byte ADCON0 = 0xFC2
#byte ADCON0 = 0xFC2
#byte ADCON1 = 0xFC1
#byte ADCON2 = 0xFC0
#word CCPR1 = 0xFBF
#byte CCP1CON = 0xFBD
#word CCPR2 = 0xFBC
#byte CCP2CON = 0xFBA
#byte BAUDCON = 0xFB8
#byte BAUDCON = 0xFB8
#byte ECCP1DEL = 0xFB7
#byte ECCP1AS = 0xFB6
#byte CVRCON = 0xFB5
#byte CVRCON = 0xFB5
#byte CMCON = 0xFB4
#word TMR3 = 0xFB3
#byte T3CON = 0xFB1
#byte T3CON = 0xFB1
#byte SPBRGH = 0xFB0
#byte SPBRG = 0xFAF
#byte RCREG = 0xFAE
#byte TXREG = 0xFAD
#byte TXSTA = 0xFAC
#byte RCSTA = 0xFAB
#byte RCSTA = 0xFAB
#byte EEADR = 0xFA9
#byte EEDATA = 0xFA8
#byte EECON2 = 0xFA7
#byte EECON1 = 0xFA6
#byte IPR2 = 0xFA2
#byte IPR2 = 0xFA2
#byte PIR2 = 0xFA1
#byte PIR2 = 0xFA1
#byte PIE2 = 0xFA0
#byte PIE2 = 0xFA0
#byte IPR1 = 0xF9F
#byte PIR1 = 0xF9E
#byte PIE1 = 0xF9D
#byte OSCTUNE = 0xF9B
#byte TRISE = 0xF96
#byte TRISD = 0xF95
#byte TRISC = 0xF94
#byte TRISC = 0xF94
#byte TRISB = 0xF93
#byte TRISA = 0xF92
#byte LATE = 0xF8D
#byte LATD = 0xF8C
#byte LATC = 0xF8B
#byte LATB = 0xF8A
#byte LATA = 0xF89
#byte PORTE = 0xF84
#byte PORTD = 0xF83
#byte PORTC = 0xF82
#byte PORTB = 0xF81
#byte PORTA = 0xF80
#byte UEP15 = 0xF7F
#byte UEP14 = 0xF7E
#byte UEP13 = 0xF7D
#byte UEP12 = 0xF7C
#byte UEP11 = 0xF7B
#byte UEP10 = 0xF7A
#byte UEP9 = 0xF79
#byte UEP8 = 0xF78
#byte UEP7 = 0xF77
#byte UEP6 = 0xF76
#byte UEP5 = 0xF75
#byte UEP4 = 0xF74
#byte UEP3 = 0xF73
#byte UEP2 = 0xF72
#byte UEP1 = 0xF71
#byte UEP0 = 0xF70
#byte UCFG = 0xF6F
#byte UADDR = 0xF6E
#byte UCON = 0xF6D
#byte USTAT = 0xF6C
#byte UEIE = 0xF6B
#byte UEIR = 0xF6A
#byte UIE = 0xF69
#byte UIR = 0xF68
#word UFRM = 0xF67
#byte SPPCON = 0xF65
#byte SPPEPS = 0xF64
#byte SPPCFG = 0xF63
#byte SPPDATA = 0xF62
