
//TARJETA ISC_40PIN
//Control de la tarjeta via puerto serial
//E0 esta conectado a un boton, entrada
//E1 esta conectado a un led, salida 
//E2 esta conectado a un boton, entrada
//Salida y entrada serial por C6 y C7
// feb13/marz14/oct14/jun15/oct15

//*************   Configuracion del PIC *****************************
#include <18f4550.h>
#include <pic18f4550_registers.h>

#fuses HS,NOWDT,NOPROTECT,NOLVP,PUT,CPUDIV1,BROWNOUT    // Configuracion del PIC: hs_cristal, No_watch dog, NO_codigo de proteccion                                           
#use delay(clock=20M)                           //Velocidad del reloj  de 12 mhz
#include <lcd.c>
#define use_portd_lcd TRUE

//#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL3,CPUDIV1,VREGEN // Funcionamiento con modulo PLL=3 para 12Mhz, PLL=5 para 20Mhz
//#use delay(clock=48000000)

#use rs232(baud=9600, xmit=PIN_C6, rcv=PIN_C7,bits=8,parity=n)  //Configuracio Pto serial
#build(reset=0x1000,interrupt=0x1008)    #org 0x0000,0x0FFF {} 


 //************************ PROGRAMA *********************************
void main()
{
  // ******************* Configuración de Entradas y Salidas *************** 
  
   TRISB = 0xFF;              //Puerrto b definido como entrada
   TRISD = 0x00;              //Puerrto D definido como salida
   
 //  ************************************************************************
   int caracter;
   lcd_init();    //lcd_putc("\Conectar el \n");   lcd_putc("\Puerto serial\n");
 
   while(1)
      {
         printf("\r\r\n Enviar caracter por puerto serial ") ;
         printf("\r\n Escribe un caracter: ") ;
         caracter =getch(); //lee el puerto serial
         printf(lcd_putc, "%c",caracter);
         putc(caracter);     // enviamos el eco 
         delay_ms(100);
       } //fin while
 
} // Fin 
///************************************


 
